# Förslag v3

## Överblick klasser
  
**Provider  <->  DataSubject  <->  ThirdParty**  
  
- Inhämtning av data sker från en **Provider**  
- **DataSubject** (individen) gör inhämtning och utlämning av datan  
- **DataSubject** kan dela datan med **ThirdParty** (konsument) och hur datan får användas finns beskrivet i ett **Consent**

***

## Överblick steg

**Steg 1:**  
En datadelning initieras av en **Request**:  
**ProviderRequest**: Begäran individen skickar till en Provider.  
**ThirdPartyRequest**: Begäran en konsument skickar till individen.  
  
**Steg 2:**  
Den som initierat en Request mottar en **Response**:  
**ProviderResponse**: Svar individen får från en Provider.  
**DataSubjectResponse**: Svar konsumenten får från individen.  
  
För en datadelning finns kopplat ett **Consent** - vad som ges tillåtelse till:  
**ThirdPartyConsent**: Tillåtelse individen ger till ThirdParty.  
**ProviderConsent**: Tillåtelse individen ger till Provider.  

**Response** innehåller en länk/URI till datan som förfrågan gäller. Denna URI slutar fungera om ett givet **ThirdPartyConsent** löper ut eller dras tillbaka.

***

## Egenskaper
Varje klass har ett antal egenskaper, se [suggestion_v3_working_copy.owl](suggestion_v3_working_copy.owl) (öppna i t ex [protégé](https://protege.stanford.edu) eller [WebVOWL](http://vowl.visualdataweb.org/webvowl.html)) samt exempel nedan.

***
## Exempel

**Klass**: ThirdPartyRequest  
**Beskrivning:** ThirdParty efterfrågar data av DataSubject.  
**Egenskaper**: data_requested, is_purpose_for_request, has_requested_expiry: time  
**Exempel:**
```json
    {   
        "data_requested": {},
        "is_purpose_for_request": "DataSharing",
        "has_requested_expiry": "2022-12-23T18:25:43.511Z",
        "": 
    }
```

**Klass**: DataSubjectResponse  
**Beskrivning:** Svar från DataSubject till ThirdParty  
**Egenskaper**: is_purpose_for_request, has_requested_expiry  
**Exempel:**
```json
    {   
        "timestamp": "",
        "is_purpose_for_request": "",
        "has_requested_expiry": "", 
    }
```

**Klass**: Status  
**Beskrivning:**   
**Egenskaper**:   
**Exempel:**
```json
    {
    }
```

**Klass**: ThirdPartyConsent  
**Beskrivning:** Consent som bifogas i svar till ThirdParty.  
**Egenskaper:** has_expiry_time, for_purpose    
**Exempel:**
```json
    {
        "has_expiry_time": "",
        "for_purpose": "",
    }
```

**Beskrivning:** Svar skickas till ThirdParty.  
**Egenskaper:**  
**Exempel:**
```json
    {
        "timestamp": "",
        "expiry_time": ""
    }
```
