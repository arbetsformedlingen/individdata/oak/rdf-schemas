Links:  

GConsent: https://openscience.adaptcentre.ie/ontologies/GConsent/docs/ontology  

GDPRtEXT: https://openscience.adaptcentre.ie/ontologies/GDPRtEXT/deliverables/docs/ontology  

Review of ontologies and policy languages in GDPR (published 7 june 2022): https://content.iospress.com/articles/semantic-web/sw223009  

Best Practices for Implementing FAIR Vocabularies and Ontologies on the Web (2020): https://dgarijo.com/papers/best_practices2020.pdf  

Exempel spec mina meddelanden + spec dcat: https://www.digg.se/download/18.51886f8917fb8a32609431f/1650446938790/tjanstespecifikation-mina-meddelanden-t.pdf  
https://www.digg.se/download/18.3b27105f17fb89447e82474/1648819278514/tekniska-tjanstekontrakt-api-v3-t.pdf  
https://docs.dataportal.se/dcat/en/

eIDAS: https://digital-strategy.ec.europa.eu/en/policies/eidas-regulation


Notes:

- Suggestion v3 try to use same namings as in GConsent and GDPRtEXT - this is the same vocabulary used in the EU GDPR articles. 
