## Tools

[EASYRDF Converter](https://www.easyrdf.org/converter) for transformation between different RDF serializations.

[JSON-LD Playground](https://json-ld.org/playground/) for visualisation, validation and transformation between differens json-ld formats.

## List of ontologies related to service description and service registry

[Cookbook for translating Data Models to RDF Schemas](https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/document/cookbook-translating-data-models-rdf-schemas) from the EU organisation Semantic Interoperability Community (SEMIC).

[OWL 2 Web Ontology Language](https://www.w3.org/TR/owl2-overview/)  

[OWL-S 1.2](http://www.ai.sri.com/~daml/services/owl-s/1.2/)  

[DAML Ontology Library](http://www.daml.org/ontologies/) A big collection of ontologies. Unfortunately useless since the links are broken!

[Web Service Modelling Ontology (WSMO)](https://www.w3.org/Submission/WSMO/). An alternative to OWL-S. More recent updates. Perhaps more complicated?  

[ESSI WSMO working group](http://www.wsmo.org/index.html). A lot of resources.  

[Core ontology for linked data registry services](https://epimorphics.com/public/vocabulary/Registry.html).  

[SKOS Simple Knowledge Organization System Reference](https://www.w3.org/TR/skos-reference/). AN ontology for cencepts and notations. Used by the registry ontology.)

## Some articles and reports

[Ontologies for E-government](http://knut.hinkelmann.ch/publications/192329_1_En_19_Chapter_OnlinePDF.pdf) Interesting chapter from a book that touches a some of the cencepts we have to implement in project Oak.

[LIFE EVENTS IN THE DESIGN OF PUBLIC SERVICES](https://aaltodoc.aalto.fi/bitstream/handle/123456789/44782/master_Gros_Christopher_2020.pdf?sequence=1&isAllowed=y). Master thesis from 2020\. References a Life Event Ontology.  

[A Conceptual Model for Semantically-based E-Government Portals](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.100.8086&rep=rep1&type=pdf). References to OWL-S, WSMO and Life Event Ontologies

[Life Event Ontology Based EGovernment Service Integration with Privacy Awareness](https://opus.lib.uts.edu.au/bitstream/10453/34451/2/02whole.pdf). Contains comparison between OWL-S and WSMO.

[Life Events – Connecting Citizens to Services](https://www.actiac.org/system/files/Life%20Events%20-%20Connecting%20Citizens%20to%20Services_112414.pdf). Advocates the usage of ontologies.
